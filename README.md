# XR Mailing app

## Install

### Using install script

```
wget https://code.dbonnet.ovh/irietux/xr-mailing/raw/branch/master/install.sh -O - | bash
```

### Using docker

```
export DOWNLOAD_DIR="/tmp/xr-mailing-docker"
export DATA_DIR="/data/xr-mailing"
mkdir -p $DOWNLOAD_DIR
mkdir -p $DATA_DIR
wget https://cloud.dbonnet.ovh/index.php/s/jYxSaYZg72cTWwx -O ${DOWNLOAD_DIR}/xr-mailing-docker.tar
docker load -i ${DOWNLOAD_DIR}/xr-mailing-docker.tar
docker run -d -p 5001:5001 -v ${DATA_DIR}:/app/data xr-mailing
```

### Manually

```
export BASE_DIR="/srv/http"
export APP_DIR="${BASE_DIR}/xr-mailing"
export USER="xr-mailing"
export REPO="https://code.dbonnet.ovh/irietux/xr-mailing"

# Setup directories
mkdir -p $BASE_DIR
cd $BASE_DIR
git clone $REPO $APP_DIR
useradd -d ${APP_DIR} -s /bin/bash $USER
mkdir ${APP_DIR}/data
chown -R ${USER}:${USER} $APP_DIR
cd $APP_DIR

# Install requirements for virtualenv
pip install virtualenvwrapper setuptools virtualenv

# Create systemd service and start it
cp xr-mailing.service /etc/systemd/system/
systemctl daemon-reload
systemctl start xr-mailing.service
```

## Reverse proxy

### Generate TLS certificate

#### With letsencrypt :

First you have to install certbot, to do it follow this documentation : https://certbot.eff.org/instructions

```
# If a web server is running on port 80 stop it
systemctl stop apache2

# Generate the certificate
certbot certonly --standalone --rsa-key-size 4096 -d xr-mailing.your-domain.fr

# If you stopped web server restart it
systemctl start apache2
```

### Apache2 reverse proxy

A sample of apache2 is provided as "xr-mailing-httpd.sample.conf".

You can copy it to your apache config directory and edit it to set your domain name.
For example :
```
cp /srv/http/xr-mailing/xr-mailing-httpd.sample.conf /etc/apache2/sites-available/xr-mailing.conf

# Edit it with vim :
vim /etc/apache2/sites-available/xr-mailing.conf

# Or with nano (if you are crazy):
nano /etc/apache2/sites-available/xr-mailing.conf
```

## Initial setup

If database is not existing it will be created at first login.

When you connect to https://xr-mailing.your-domain.fr/ you arrived on the initial setup page.

This page contains a form to set the local group name and to create the first admin.

When you filled the form click on validate. Your xr-mailing instance is ready.

## LICENCE

Copyright © 2019 Dorian Bonnet

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

xr-mailing is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with IrieBar.  If not, see <https://www.gnu.org/licenses/>.
