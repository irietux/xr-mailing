# FROM tiangolo/meinheld-gunicorn-flask:python3.7
FROM frolvlad/alpine-python3:latest
RUN mkdir -p /srv/http
MAINTAINER IrieTux "irietux@protonmail.com"
COPY . /app
WORKDIR /app
EXPOSE 5001
RUN mkdir -p /app/data
RUN pip install virtualenvwrapper setuptools virtualenv
RUN pip install -r /app/requirements.txt
CMD ["/usr/bin/gunicorn", "--access-logfile", "-", "--workers", "3", "--bind", "0.0.0.0:5001", "main:app"]
