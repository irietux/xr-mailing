#!/bin/bash

export BASE_DIR="/srv/http"
export APP_DIR="${BASE_DIR}/xr-mailing"
export USER="xr-mailing"
export REPO="https://code.dbonnet.ovh/irietux/xr-mailing"

# Setup directories
mkdir -p $BASE_DIR
cd $BASE_DIR
git clone $REPO $APP_DIR
useradd -d ${APP_DIR} -s /bin/bash $USER
mkdir ${APP_DIR}/data
chown -R ${USER}:${USER} $APP_DIR
cd $APP_DIR

# Install requirements for virtualenv
pip install virtualenvwrapper setuptools virtualenv

# Create systemd service and start it
cp xr-mailing.service /etc/systemd/system/
systemctl daemon-reload
systemctl start xr-mailing.service
