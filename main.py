#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright © 2019 Dorian Bonnet
#
# This file is part of xr-mailing.
#
# xr-mailing is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# IrieBar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with xr-mailing.  If not, see <https://www.gnu.org/licenses/>

__author__ = "irietux"
__version__ = "1"
__email__ = "irietux@protonmail.com"

from mailing import app
import os

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)

