#!/bin/bash

# Copyright © 2019 Dorian Bonnet
#
# This file is part of xr-mailing.
#
# xr-mailing is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# IrieBar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with xr-mailing.  If not, see <https://www.gnu.org/licenses/>

cd $(realpath $(dirname $0))

# Setup venv
export WORKON_HOME=~/Envs
mkdir -p $WORKON_HOME
source $(whereis virtualenvwrapper.sh | cut -d' ' -f2)

# Start venv
workon flask_env
if [ $? -ne 0 ]; then
  mkvirtualenv --python=$(which python3) flask_env
fi
pip3 install -r requirements.txt

# Start server
gunicorn --access-logfile - --workers 3 --bind 127.0.0.1:5001 main:app
