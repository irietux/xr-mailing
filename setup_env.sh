#!/bin/bash
export WORKON_HOME='~/Envs'
mkdir -p ${WORKON_HOME}
. $(whereis virtualenvwrapper.sh | cut -d' ' -f2)
mkvirtualenv flask_env

# Install requirements
pip3 install -r requirements.txt