#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright © 2019 Dorian Bonnet
#
# This file is part of xr-mailing.
#
# xr-mailing is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# IrieBar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with xr-mailing.  If not, see <https://www.gnu.org/licenses/>

from flask import Flask, request, render_template, redirect, url_for, flash, session, abort
import json, re, os

from mailing import app
from mailing.models.Mailing import Model

model = Model()

keyFile = ".secret_key"
def readKey():
    with open(keyFile, 'r') as f:
        a = []
        s = f.read()
        for e in s.split():
            a.append(int(e))
        key = bytes(a)
    return key
def writeKey(key):
    # Convert key to string
    s = ""
    for e in key:
        s += str(int(e)) + " "
    # Write it
    with open(keyFile, 'w') as f:
        f.write(s)
def genKey():
    return os.urandom(24)

if not os.path.isfile(keyFile):
    key = genKey()
    writeKey(key)
else:
    key = readKey()
app.secret_key = key    

@app.route('/')
def home():
    if model.checkNeedSetup():
        return render_template('setup.html')
      
    groupName = model.getGroupName()

    if not session.get('logged_in'):
        error = False
        e = request.args.get("error")
        if e == "1":
            error = True
        return render_template('login.html', groupName = groupName, error = error, login = True)

    return listPeople()
    
@app.route('/people')
def people():
    mode = "people"

    if not session.get('logged_in'):
        return home()

    people = model.list()

    groupName = model.getGroupName()
    updatePerson = people[0]
    updateEmail = request.args.get("updateEmail")
    if updateEmail:
        updatePerson = model.getByEmail(updateEmail)
    
    return render_template('people.html', people = people, updatePerson = updatePerson, groupName = groupName, mode = mode)

@app.route('/admins')
def admins():
    mode = "admin"
    if not session.get('logged_in'):
        return home()

    groupName = model.getGroupName()
    people = model.list()
    admins = model.listAdmins()

    return render_template('admins.html', people = people, admins = admins, groupName = groupName, mode = mode)

@app.route('/list')
def listPeople():
    mode = "list"
    groupName = model.getGroupName()

    if not session.get('logged_in'):
        return home()

    people = model.list()
    listMode = request.args.get('listMode')
    circle1 = request.args.get('circle1')
    circle2 = request.args.get('circle2')
    circle3 = request.args.get('circle3')

    # Create a list of every requested circles
    current_circles = [int(c) for c in (circle1, circle2, circle3) if c is not None]
    if not current_circles:
        # If the list of circles is empty, then we want all circles
        current_circles = [1, 2, 3]

    # Total number of displayed people
    total = sum(1 for person in people if person[3] in current_circles)

    return render_template('list.html', groupName = groupName, mode = mode, people = people, listMode = listMode, circle1 = circle1, circle2 = circle2, circle3 = circle3, current_circles = current_circles, total = total)

@app.route('/login', methods=['POST'])
def do_admin_login():
    if model.isAdmin(request.form['username'], request.form['password']):
        session['logged_in'] = True
        return redirect(url_for('home'))
    else:
        flash('wrong password!')
        return redirect(url_for('home')+"?error=1")

@app.route('/logout')
def logout():
    session['logged_in'] = False
    return redirect(url_for('home'))

@app.route("/setup", methods=['POST'])
def setup():

    group = str(request.form['group'])
    email = str(request.form['email'])
    name = str(request.form['name'])
    tel = str(request.form['tel'])
    circle = int(request.form['circle'])
    password = str(request.form['password'])

    model.setGroupName(group)
    model.add(name, email, circle, tel)
    model.addAdmin(email, password)

    return redirect(url_for('home'))

@app.route("/add", methods=['POST'])
def add():
    if not session.get('logged_in'):
        return home()

    email = str(request.form['email'])
    name = str(request.form['name'])
    tel = str(request.form['tel'])
    circle = int(request.form['circle'])

    if not re.match(r"[^@]+@[^@]+\.[^@]+", email):
        return "ko : address invalid"
    if (circle < 1)&(circle > 3):
        return "ko : circle number"
            
    model.add(name, email, circle, tel)
    return redirect(url_for('people'))

@app.route("/del", methods=['POST'])
def rm():
    if not session.get('logged_in'):
        return home()


    email = request.form['email']
    model.delete(email) 
    return redirect(url_for('people'))

@app.route("/update", methods=['POST'])
def update():
    if not session.get('logged_in'):
        return home()


    email = str(request.form['email'])
    newEmail = str(request.form['newemail'])
    name = str(request.form['name'])
    tel = str(request.form['tel'])
    circle = int(request.form['circle'])

    model.update(email, name, circle, tel, newEmail)
    return redirect(url_for('people'))

@app.route("/list")
def list():
    if not session.get('logged_in'):
        return home()

    circle = None
    c = request.args.get('circle')
    if c != None:
        circle = int(c)
        if (circle < 1)&(circle > 3):
            return "ko : circle number is invalid"
    data = model.list(circle)
    return json.dumps(data)

@app.route("/addAdmin", methods=['POST'])
def addAdmin():
    if not session.get('logged_in'):
        return home()


    email = request.form['email']
    password = request.form['password']
    model.addAdmin(email, password)
    return redirect(url_for('admins'))

@app.route("/delAdmin", methods=['POST'])
def delAdmin():
    if not session.get('logged_in'):
        return home()

    email = request.form['email']
    model.delAdmin(email)
    return redirect(url_for('admins'))

@app.route("/updateAdmin", methods=['POST'])
def updateAdmin():
    if not session.get('logged_in'):
        return home()


    email = request.form['email']
    password = request.form['password']
    model.updateAdmin(email, password)
    return redirect(url_for('admins'))
    
@app.route("/listAdmins")
def listAdmins():
    if not session.get('logged_in'):
        return home()


    data = model.listAdmins()
    return json.dumps(data)

