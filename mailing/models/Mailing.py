#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright © 2019 Dorian Bonnet
#
# This file is part of xr-mailing.
#
# xr-mailing is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# IrieBar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with xr-mailing.  If not, see <https://www.gnu.org/licenses/>

import os, sqlite3, hashlib
from passlib.apps import custom_app_context as pwd_context
class Model:
    def __init__(self, dbName="data/database.db"):
        self.dbName = dbName

    def checkNeedSetup(self):
        if not self.getGroupName():
            self.prepare()
            return True
        else:
            return False
    
    def add(self, name, email, circle, tel=None):
        try:
            conn = sqlite3.connect(self.dbName)
            c = conn.cursor()
            if tel:
                c.execute("INSERT INTO people (name, email, circle, tel) VALUES (?,?,?,?)", (name, email, circle, tel))
            else:
                c.execute("INSERT INTO people (name, email, circle) VALUES (?,?,?)", (name, email, circle))
            conn.commit()
            conn.close()
            return 0
        except sqlite3.IntegrityError as e:
            if str(e) == "UNIQUE constraint failed: people.email":
                return -1
            return -2
        except:
            return -3

    def update(self, email, name, circle, tel, newEmail):
        conn = sqlite3.connect(self.dbName)
        c = conn.cursor()
        c.execute("SELECT * FROM people WHERE email = ?", (email,))
        person = c.fetchone()
        c.execute("UPDATE people SET name = ?, circle = ?, tel = ?, email = ? WHERE id = ?", (name, circle, tel, newEmail, person[0],))
        conn.commit()
        conn.close()

    def list(self, circle=None):
        try:
            conn = sqlite3.connect(self.dbName)
            c = conn.cursor()
            if circle:
                c.execute("SELECT * FROM people LEFT OUTER JOIN admins ON admins.id = people.id WHERE circle = ? ORDER BY email", (circle,))
            else:
                c.execute("SELECT * FROM people LEFT OUTER JOIN admins ON admins.id = people.id ORDER BY email")
            data = c.fetchall()
            conn.close()
            return data
        except:
            return -1

    def getByEmail(self, email):
        try:
            conn = sqlite3.connect(self.dbName)
            c = conn.cursor()
            c.execute("SELECT * FROM people LEFT OUTER JOIN admins ON admins.id = people.id WHERE email = ? LIMIT 1", (email,))
            data = c.fetchone()
            conn.close()
            return data
        except:
            return -1

    def delete(self, email):
        try:
            conn = sqlite3.connect(self.dbName)
            c = conn.cursor()
            c.execute("DELETE FROM people WHERE email = ?", (email,))
            conn.commit()
            conn.close()
        except:
            pass

    def addAdmin(self, email, password):
        try:
            conn = sqlite3.connect(self.dbName)
            c = conn.cursor()
            c.execute("SELECT * FROM people WHERE email = ?", (email,))
            user = c.fetchone()
            c.execute("INSERT INTO admins (password, id) VALUES (?,?)", (self.hashPassword(password.encode('utf-8')), user[0]))
            conn.commit()
            conn.close()
            return 0
        except Exception as e:
            return -1

    def delAdmin(self, email):
        try:
            conn = sqlite3.connect(self.dbName)
            c = conn.cursor()
            c.execute("SELECT * FROM people WHERE email = ?", (email,))
            admin = c.fetchone()
            c.execute("DELETE FROM admins WHERE id = ?", (admin[0], ))
            conn.commit()
            conn.close()
            return 0
        except:
            return -1

    def updateAdmin(self, email, password):
        try:
            conn = sqlite3.connect(self.dbName)
            c = conn.cursor()
            c.execute("SELECT * FROM people WHERE email = ?", (email,))
            admin = c.fetchone()
            c.execute("UPDATE admins SET password = ? WHERE id = ?", (self.hashPassword(password.encode('utf-8')), admin[0],))
            conn.commit()
            conn.close()
            return 0
        except:
            return -1

    def hashPassword(self, plaintextPassword):
        try:
            return pwd_context.hash(plaintextPassword)
        except Exception as e:
            print(e)
            return None

    def checkPassword(self, plaintextPassword, hashedPassword):
        res = None
        try:
            res = pwd_context.verify(plaintextPassword, hashedPassword)
        except:
            pass
        return res

    def listAdmins(self):
        try:
            conn = sqlite3.connect(self.dbName)
            c = conn.cursor()
            c.execute("SELECT * FROM people LEFT OUTER JOIN admins ON admins.id = people.id WHERE password != '' ORDER BY email")
            admins = c.fetchall()
            conn.close()
            return admins
        except:
            return -1

    def isAdmin(self, email, password):
        #  try:
        conn = sqlite3.connect(self.dbName)
        c = conn.cursor()
        c.execute("SELECT * FROM people LEFT OUTER JOIN admins ON admins.id = people.id WHERE email = ?", (email, ))
        admin = c.fetchone()
        if admin:
            return self.checkPassword(password, admin[6])
        else:
            return False
    
    def setGroupName(self, name):
        conn = sqlite3.connect(self.dbName)
        c = conn.cursor()
        c.execute("INSERT INTO settings (groupName) VALUES (?)", (name,))
        conn.commit()
        conn.close()

    def getGroupName(self):
        try:
            conn = sqlite3.connect(self.dbName)
            c = conn.cursor()
            c.execute("SELECT groupName FROM settings LIMIT 1")
            groupName = c.fetchone()
            return groupName[0]
        except:
            return None

    def prepare(self):
        try:
            conn = sqlite3.connect(self.dbName)
            c = conn.cursor()
            c.execute("CREATE TABLE people (id INTEGER PRIMARY KEY, name TEXT, email TEXT NOT NULL UNIQUE, circle INT NOT NULL, tel TEXT)") 
            conn.commit()
            c.execute("CREATE TABLE admins (idAdmin INTEGER PRIMARY KEY, password NOT NULL, id INTEGER NOT NULL, FOREIGN KEY (id) REFERENCES people(id))") 
            conn.commit()
            c.execute("CREATE TABLE settings (groupName TEXT NOT NULL)")
            conn.commit()
            conn.close()
        except:
            pass
